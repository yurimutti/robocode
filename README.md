# Objetivo

Robô em Java para o Talent Sprint Solutis 2020.

## Estratégia

Escolhi as cores em azul para o C_3PO, que foi programado para ser bem rápido e agressivo mais que ao mesmo tempo também fosse inteligente e controlasse seu poder de disparo de acordo com sua energia. Outro ponto positivo é que quando bem perto do inimigo ele dispara um tiro de força máxima já que a taxa acerto vai ser bem maior. No final ele faz uma dancinha de comemoração.

- Pontos negativos

Apesar de ser rápido apresenta alguns problemas na mobilidade que fazem ele travar na parede.

## Minha Experiência

O Talent Sprint Solutis 2020 foi bastante importante para mim, gostei muito de toda mecânica do desafio, minha experiência foi bem positiva, ainda não tinha feito nada com Java o que me motivou em buscar mais informações e tentar fazer o meu máximo.

