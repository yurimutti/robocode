package solutis;
import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * C_3PO - a robot by Yuri Mutti
 */
public class C_3PO extends AdvancedRobot
{
	/**
     * run: executado quando o round for iniciado
     */
	public void run() {
		// Initialization of the robot should be put here
		// setColors(Color.red,Color.blue,Color.green); // body,gun,radar
		setBodyColor(Color.blue);  // Body => Corpo
		setGunColor(Color.lightGray); // Gun => Arma
		setRadarColor(Color.blue); // Radar => Radar
		
		setScanColor(Color.darkGray); // Scan => Varredura
		setBulletColor(Color.blue); // Bullet => Bala

		// Robot main loop
		while(true) {
			// Replace the next 4 lines with any behavior you would like
			
		    setAhead(100);
			setTurnRight(90);
			setTurnGunRight(90);
			execute();
		}
	}

	/**
     * onScannedRobot: Executado quando o radar encontra um robo.
     */
	public void onScannedRobot(ScannedRobotEvent e) {
			if (getEnergy() > 20)
			{
				fire(3);
			}
			else
			{
				fire(1);
			}
	}

	/**
	 * onHitByBullet: É executado quando o robô leva um tiro.
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
		turnRight(45);
		back(100);
	}
	
	/**
	 * onHitWall: É executado quando o robô colide com a parede.
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		turnLeft(180);
	}	
	/**
	 * onHitRobot: É executado quando o robô bate em outro robô..
	 */
	public void onHitRobot(HitRobotEvent INI) {
		turnRight(INI.getBearing()); 
		fire(3);
	}

	/**
	 * onWin: É executado quando o robô ganha o round.
	 */
	public void onWin(WinEvent e) {
        int wohoo = 1;
        while (true) {
            turnRight(180 * wohoo);
            wohoo = wohoo * -1;
        }
    }
}
